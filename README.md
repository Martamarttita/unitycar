# UnityCar

UnityCar project

A la hora de realizar el proyecto se han tenido en cuenta los siguientes puntos:

    -Se han cambiado todos los Canvas de las escenas, tanto los creados como los ya existentes, para que fueran responsive.
     Para ello se ha establecido el atributo "UI Scale Mode" con el valor "Scale width Screen Size".
     Posteriormente se ajustaron los anclas de cada uno de los componente gráficos.
     
    -Se ha considerado determinar como récord el tiempo de la mejor vuelta hasta el momento realizada por el usuario a lo 
     largo de todas las partidas jugadas, no de la partida actual mientras el juego permanece en memoria. 
     
    -Para el almacenamiento local de los datos se ha utilizado PlayerPrefs. 
    
    -Controles del juego en el móvil:
     Partiendo del enunciado del documento, se ha fijado la aceleración constante del coche, dejando a disposición del usuario los controles de giro. 
     Esto se ha logrado haciendo que si el usuario mantiene pulsado con los dedos una mitad de la pantalla (izquierda o derecha), el coche realice el 
     giro hacia dicho lado.
     Por defecto los controles son el input de teclado, para usar el input del mobil se debe arrastrar el componenete CarControllerInput a la variable "Input"
     del script "Kart Movement" que está en el GameObject "Kart".
     
    -Para generar un APK con soporte de arquitectura ARM64 en el apartado Other Setting, se establece el Scripting Backend como IL2CPP
     y el Target Architecture activado el ARM64.

    