﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;


public class UIManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private TextMeshProUGUI labelRecord;
    [SerializeField] private TextMeshProUGUI labelRecordValue;
    [SerializeField] private TextMeshProUGUI labelCoinsValue;





    /// <summary>
    /// Shows the best time lap.
    /// </summary>
    /// <param name="bestLap">Best lap.</param>
    public void ShowBestTimeLap(float bestLap)
    {
        labelRecord.gameObject.SetActive(true);
        labelRecordValue.text = System.Math.Round(bestLap, 3) + " seg";
        labelRecordValue.gameObject.SetActive(true);
    }
   
    /// <summary>
    /// Shows the total coins.
    /// </summary>
    /// <param name="totalCoins">Total coins.</param>
    public void ShowTotalCoins(int totalCoins)
    {
        labelCoinsValue.text = totalCoins + "";
    }

    /// <summary>
    /// Change Scene
    /// </summary>
    /// <param name="nameScene">the scene name to change </param>
    public void ButtonPlay(string nameScene)
    {
        SceneManager.LoadScene(nameScene, LoadSceneMode.Single);
    }



    /// <summary>
    /// Exit Application 
    /// </summary>
    public void ButtonExit()
    {
        Application.Quit();
    }
}
