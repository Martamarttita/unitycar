﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{

    private UIManager _uiManager;

    // Start is called before the first frame update
    void Start()
    {
        _uiManager=this.GetComponent<UIManager>();
        float bestLap= LocalDataManager.Instance.GetBestLap();
        int totalCoins = LocalDataManager.Instance.GetTotalCoins();
        if(bestLap >0){
            _uiManager.ShowBestTimeLap(bestLap);
        }
        if(totalCoins>0){
            _uiManager.ShowTotalCoins(totalCoins);
        }


    }

   

}
