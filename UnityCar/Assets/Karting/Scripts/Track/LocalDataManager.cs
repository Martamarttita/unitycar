﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalDataManager 
{




    #region SINGLETON PATTERN
    public static LocalDataManager _instance;
    public static LocalDataManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new LocalDataManager();

                
            }

            return _instance;
        }
    }
	#endregion

   


    /// <summary>
    /// Set the besttotal lap
    /// </summary>
    /// <param name="timeLap"></param>
    public void SetBestLap(float timeLap)
    {
        float dataBestLap = PlayerPrefs.GetFloat("BestTime");

        if (timeLap < dataBestLap || dataBestLap == 0)
        {
            PlayerPrefs.SetFloat("BestTime", timeLap);

        }

    }

    /// <summary>
    /// Gets the best lap.
    /// </summary>
    /// <returns>The best lap.</returns>
    public float GetBestLap()
    {
       return PlayerPrefs.GetFloat("BestTime");

    }

    /// <summary>
    /// Sets the best coins.
    /// </summary>
    /// <param name="coins">Coins.</param>
    public void SetBestCoins(int coins)
    {
        PlayerPrefs.SetInt("TotalCoins", GetTotalCoins()+coins);

    }

    /// <summary>
    /// Gets the total coins.
    /// </summary>
    /// <returns>The total coins.</returns>
    public int GetTotalCoins()
    {
        return PlayerPrefs.GetInt("TotalCoins");

    }

}
