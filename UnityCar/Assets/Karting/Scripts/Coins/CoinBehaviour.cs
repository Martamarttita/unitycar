﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }



    /// <summary>
    /// Get Coin 
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        CoinsManager.Instance.TotalCoins++;
        CoinsManager.Instance.PutCoinsUI();
        Destroy(this.gameObject);

    }



}
