﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoinsManager : MonoBehaviour
{

    [SerializeField] private int totalCoins;
    [SerializeField] private TextMeshProUGUI coinsTextUI;
    private int numCoins;

    #region SINGLETON PATTERN
    public static CoinsManager _instance;
    public static CoinsManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<CoinsManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("CoinsManager");
                    _instance = container.AddComponent<CoinsManager>();
                }
            }

            return _instance;
        }
    }

    #endregion

    public int TotalCoins { get => totalCoins; set => totalCoins = value; }


	public void Start()
	{
         GameObject[] coins=GameObject.FindGameObjectsWithTag("COIN");
        numCoins = coins.Length;

	}

	public void  PutCoinsUI()
    {
        coinsTextUI.text = "Total Coins: " + totalCoins +"/"+numCoins;
    }
}
