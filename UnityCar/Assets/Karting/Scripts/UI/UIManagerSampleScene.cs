﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class UIManagerSampleScene : MonoBehaviour
{

    [SerializeField] private GameObject canvasRecord;

    #region SINGLETON PATTERN
    public static UIManagerSampleScene _instance;
    public static UIManagerSampleScene Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<UIManagerSampleScene>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("UIManagerSampleScene");
                    _instance = container.AddComponent<UIManagerSampleScene>();
                }
            }

            return _instance;
        }
    }

    #endregion

    public void ShowCanvasRecord()
    {
        StartCoroutine(ShowCanvas(2.0f));
    }
    private IEnumerator ShowCanvas(float waitTime)
    {
        canvasRecord.SetActive(true);
        yield return new WaitForSeconds(waitTime);
        canvasRecord.SetActive(false);
    }


    /// <summary>
    /// Changes the scene.
    /// </summary>
    /// <param name="nameScene">Name scene.</param>
    public void ChangeScene(string nameScene)
    {
        SceneManager.LoadScene(nameScene, LoadSceneMode.Single);

    }

   


}
