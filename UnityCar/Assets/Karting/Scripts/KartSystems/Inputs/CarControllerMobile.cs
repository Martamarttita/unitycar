﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace KartGame.KartSystems
{
    public class CarControllerMobile : MonoBehaviour,IInput
    {
        private Rect p1Zone;
        private Rect p2Zone;



        #region SINGLETON PATTERN
        public static CarControllerMobile _instance;
        public static CarControllerMobile Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<CarControllerMobile>();

                    if (_instance == null)
                    {
                        GameObject container = new GameObject("CarControllerMobile");
                        _instance = container.AddComponent<CarControllerMobile>();
                    }
                }

                return _instance;
            }
        }
        #endregion


        public float Acceleration
        {
            get { return m_Acceleration; }
        }
        public float Steering
        {
            get { return m_Steering; }
        }
        public bool BoostPressed
        {
            get { return m_BoostPressed; }
        }
        public bool FirePressed
        {
            get { return m_FirePressed; }
        }
        public bool HopPressed
        {
            get { return m_HopPressed; }
        }
        public bool HopHeld
        {
            get { return m_HopHeld; }
        }

        float m_Acceleration;
        float m_Steering;
        bool m_HopPressed;
        bool m_HopHeld;
        bool m_BoostPressed;
        bool m_FirePressed;



        // Start is called before the first frame update
        void Start()
        {
            p1Zone = new Rect(0, 0, Screen.width * 0.5f, Screen.height);
            p2Zone = new Rect(Screen.width * 0.5f, 0, Screen.width * 0.5f, Screen.height);
            m_Acceleration = 1f;
        }

        // Update is called once per frame
        void Update()
        {
            m_Steering = 0f;

            foreach (Touch touch in Input.touches)
            {
                if (p1Zone.Contains(touch.position))
                {
                    m_Steering = -1f;

                }
                else
                {
                    if (p2Zone.Contains(touch.position))
                    {
                        m_Steering = 1f;

                    }
                
                }
                
            }

        }

        public void EnableControll()
        {
            gameObject.GetComponent<CarControllerMobile>().enabled = true;
        }
    }
}
